package main;

public class Loancalc {


	private Integer annual;
	private Integer balance;
	public Integer getAnnual() {
		return annual;
	}

	public void setAnnual(Integer annual) {
		if(annual < 0) {
			throw new AnnualException();
		}
		this.annual = annual;
	}

	public int mannual() {
		return this.annual/12;
	}

	public Integer getBalance() {
		return balance;
	}

	public void setBalance(Integer balance) {
		this.balance = balance;
	}

	/**
	 * 月あたりの利息を計算する
	 *
	 * @return 1か月後の利息
	 */
	public Integer calcInterest() {
		return this.balance*mannual()/100;
	}

	public void payback(int i) {
		i -= this.calcInterest();
		this.balance -= i;
	}

	public void payback(int i, Integer calcInterest) {
		// TODO 自動生成されたメソッド・スタブ

	}

}
