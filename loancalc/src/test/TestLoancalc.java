package test;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import main.AnnualException;
import main.Loancalc;

import org.junit.Test;

public class TestLoancalc {

	@Test
	public void annual() {
		Loancalc lc = new Loancalc();
		lc.setAnnual(12);
		assertThat(lc.getAnnual(),is(12));
		lc.setAnnual(18);
		assertThat(lc.getAnnual(), is(18));
	}

	@Test(expected = AnnualException.class)
	public void annualerror() {
		Loancalc lc = new Loancalc();
		lc.setAnnual(-1);
	}

	@Test
	public void testCalcInterest() {
		Loancalc lc = new Loancalc();
		lc.setBalance(100000);
		lc.setAnnual(12);
		assertThat(lc.calcInterest(), is(1000));
		lc.setAnnual(24);
		assertThat(lc.calcInterest(), is(2000));
	}

	@Test
	public void testPayback() {
		Loancalc lc = new Loancalc();
		lc.setBalance(100000);
		lc.setAnnual(12);
		lc.payback(10000+lc.calcInterest());
		assertThat(lc.getBalance(), is(90000));
		lc.payback(10000+lc.calcInterest());
		assertThat(lc.getBalance(), is(80000));
		lc.payback(10000+lc.calcInterest());
		assertThat(lc.getBalance(), is(70000));
		lc.payback(10000+lc.calcInterest());
		assertThat(lc.getBalance(), is(60000));
		lc.payback(10000+lc.calcInterest());
		assertThat(lc.getBalance(), is(50000));
	}

	@Test
	public void testPaybackSep() {
		Loancalc lc = new Loancalc();
		lc.setBalance(100000);
		lc.setAnnual(12);
		lc.payback(10000, lc.calcInterest());
		assertThat(lc.getBalance(), is(90000));
	}
}
